import os

start_dir = 'cosmosis-ska-forecasting/start/'

experiments = ['cfhtlens', 'des', 'euclid', 'ska1-med', 'ska2-large', 'eucliduska2']

nz_experiments = {
    'cfhtlens': 'cfhtlens',
    'des' : 'des',
    'euclid' : 'euclid',
    'ska1-med':'ska1_med',
    'ska2-large':'ska2_large',
    'eucliduska2':'eucliduska2'
}


template = open("cosmosis-ska-forecasting/launch.template").read()

for expt in experiments:
  name = '{0}'.format(expt)
  nz_expt = nz_experiments[expt]
  ini = 'cosmosis-ska-forecasting/configuration/{0}.ini'.format(name)
  out = 'cosmosis-ska-forecasting/results/{0}.txt'.format(name)
  data_dir = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
  #		start_file='{0}/{1}.txt'.format(start_dir, name)
  cmd = 'cosmosis --mpi {0} '.format(ini)
  sub = template.format(cmd=cmd,name=name)
  sub_filename = 'cosmosis-ska-forecasting/launch/{0}.sub'.format(name)
  open(sub_filename,'w').write(sub)
  #print "qsub ", sub_filename

  w_ini = 'cosmosis-ska-forecasting/w-configuration/{0}.ini'.format(name)
  w_cmd = 'cosmosis --mpi {0} '.format(w_ini)
  w_sub = template.format(cmd=w_cmd,name='w_'+name)
  w_sub_filename =  'cosmosis-ska-forecasting/launch/w_{0}.sub'.format(name)
  open(w_sub_filename,'w').write(w_sub)
  #print "qsub ", w_sub_filename

  w0wa_ini = 'cosmosis-ska-forecasting/w0wa-configuration/{0}.ini'.format(name)
  w0wa_cmd = 'cosmosis --mpi {0} '.format(w0wa_ini)
  w0wa_sub = template.format(cmd=w0wa_cmd,name='w0wa_'+name)
  w0wa_sub_filename =  'cosmosis-ska-forecasting/launch/w0wa_{0}.sub'.format(name)
  open(w0wa_sub_filename,'w').write(w0wa_sub)
  #print "qsub ", w0wa_sub_filename

  mg_ini = 'cosmosis-ska-forecasting/mg-configuration/{0}.ini'.format(name)
  mg_cmd = 'cosmosis --mpi {0} '.format(mg_ini)
  mg_sub = template.format(cmd=mg_cmd,name='mg_'+name)
  mg_sub_filename =  'cosmosis-ska-forecasting/launch/mg_{0}.sub'.format(name)
  open(mg_sub_filename,'w').write(mg_sub)
  print "qsub ", mg_sub_filename
