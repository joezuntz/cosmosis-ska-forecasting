import numpy as np
from numpy import pi
from cosmosis.datablock import option_section, names as section_names

def setup(options):
	# Read any options and perform any setup that is needed.
	# Save any data to be passed on later into config (which
	# can be any object).
	output_file = options[option_section,'output_file']
	output_ell = options[option_section,'output_ell']
	output_covmat = options[option_section,'output_covmat']
	ellmin = options[option_section,'ellmin']
	ellmax = options[option_section,'ellmax']
	nell = options[option_section, 'nell']
	fsky = options[option_section,'fsky']
	n = options[option_section,'galaxy_density']  # per square arcmin
	sigma_e = options[option_section, 'sigma_e']  # per square arcmin
	# 1 steradian = (180/pi)**2 square degrees
	#             = (180*60/pi)**2 square arcmin
	n_steradian = n * (180.*60./pi)**2

	# Use the option getting methods on options to read options.
	config = [output_file, output_ell, output_covmat, 
	          ellmin, ellmax, nell, 
	          fsky, n_steradian, sigma_e]
	return config



def execute(block, config):
	# get survey info and output requirement
	(output_file, output_ell, output_covmat, 
		ellmin, ellmax, nell, 
 		fsky, n_steradian, sigma_e) = config
	ell_out = np.logspace(np.log10(ellmin), np.log10(ellmax), nell).astype(int)
	delta_ell = np.diff(ell_out)
	delta_ell = np.concatenate([[delta_ell[0]], delta_ell])


	# Read parameters and data from package.
	section = section_names.shear_cl
	nbin = block[section, "NBIN"]
	ell = block[section, "ELL"]

	ngal_bin = n_steradian / nbin
	CL_OBS = {}
	CL_OUT = {}
	COV = {}
	bins = list(range(1,nbin+1))
	bin_pairs = [(i,j) for i in bins for j in bins if j<=i]

	# Get the C_ell for each bin pair by interpolation
	# into the theory
	for (i,j) in bin_pairs:
		name='BIN_%d_%d' % (i,j)
		cl = block[section, name]
		cl_out = np.exp(np.interp(np.log(ell_out), np.log(ell), np.log(cl)))
		cl_obs = cl_out.copy()
		# we use cl_out for the output data 
		# and cl_obs for computing the covmat
		# They are only unequal if there is shot noise
		# in the auto-correlation bins
		if i==j:
			cl_obs += sigma_e**2 / ngal_bin
		#save both
		CL_OBS[(i,j)] = cl_obs
		CL_OUT[(i,j)] = cl_out
		#Symmetry...
		CL_OBS[(j,i)] = cl_obs
		CL_OUT[(j,i)] = cl_out
	# Get the covariance matrix
	for (i,j) in bin_pairs[:]:
		for (m,n) in bin_pairs[:]:
			#  using Takada and Jain formula
			cl_part = CL_OBS[(i,m)]*CL_OBS[(j,n)]+CL_OBS[(i,n)]*CL_OBS[(j,m)]
			cov = cl_part / (2*ell_out+1) / delta_ell / fsky
			COV[(i,j,m,n)] = cov

	# Save the ell values
	np.savetxt(output_ell, ell_out)

	# Save the simulated c_ell without noise
	f = open(output_file,'w')
	f.write('# bin_1 bin_2 cl_bin1_bin2_ell1  cl_bin1_bin2_ell2 ... \n')
	for (i,j) in bin_pairs:
		cl = CL_OUT[(i,j)]
		row = '  '.join(str(x) for x in cl)
		f.write('%d  %d  %s\n' % (i,j,row))
	f.close()

	#Save the cov mat chunk diagonals
	f = open(output_covmat,'w')
	for (i,j) in bin_pairs[:]:
		for (m,n) in bin_pairs[:]:
			cov = COV[(i,j,m,n)]
			row = '  '.join(str(x) for x in cov)
			f.write('%d  %d  %d  %d  %s\n' % (i,j,m,n,row))
	f.close()
	return 0