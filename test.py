import sys
sys.path.append("./number_density/")
import ska_number_density as ska
import photometric_smail as photo
import numpy as np

z, nz_true = np.loadtxt("./distributions/nofz_ska1_1000.dat").T
tmp = np.arange(0,10.0,0.01)
nz_true=np.interp(tmp, z, nz_true)
z = tmp
del tmp

nbin = 10
ngal = 4.2
f_spec = 0.15
z_max_spec = 0.6
sigma_z = 0.05
bias = 0.0

edges, bin_nz = ska.compute_nz(z, nz_true, nbin, ngal, sigma_z, f_spec, z_max_spec)
import pylab

for b in bin_nz:
	pylab.plot(z, b)
pylab.show()