import os


experiments = ['cfhtlens', 'des', 'euclid', 'ska1-med', 'ska2-large', 'eucliduska2']

nz_experiments = {
    'cfhtlens': 'cfhtlens',
    'des' : 'des',
    'euclid' : 'euclid',
    'ska1-med':'ska1_med',
    'ska2-large':'ska2_large',
    'eucliduska2':'eucliduska2'
}

for expt in experiments:
	ini = 'cosmosis-ska-forecasting/configuration/{0}.ini'.format(expt)
	dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(expt)
	try:
		os.mkdir(dirname)
	except:
		pass
	#print 'cosmosis {0}'.format(ini)
	
	w_ini = 'cosmosis-ska-forecasting/w-configuration/{0}.ini'.format(expt)
	w_dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(expt)
	try:
		os.mkdir(dirname)
	except:
		pass
	#print 'cosmosis {0}'.format(w_ini)
	
	w0wa_ini = 'cosmosis-ska-forecasting/w0wa-configuration/{0}.ini'.format(expt)
	w0wa_dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(expt)
	try:
		os.mkdir(dirname)
	except:
		pass
	#print 'cosmosis {0}'.format(w0wa_ini)

	mg_ini = 'cosmosis-ska-forecasting/mg-configuration/{0}.ini'.format(expt)
	mg_dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(expt)
	try:
		os.mkdir(dirname)
	except:
		pass
	print 'cosmosis {0}'.format(mg_ini)
