from cosmosis.datablock import names as section_names, option_section
import numpy as np
import photometric_smail as photo


def setup(options):
	filename = options[option_section, "filename"]
	ngal = options[option_section, "ngal"]
	spectro_fraction = options[option_section, "spectro_fraction"]
	spectro_zmax = options[option_section, "spectro_zmax"]
	photo_zmax = options[option_section, "photo_zmax"]
	photo_err = options[option_section, "photo_err"]
	no_photo_err = options[option_section, "no_photo_err"]
	nbin = options[option_section, "nbin"]
	z, nz = np.loadtxt(filename).T
	zmax=z.max()
	z_interp = np.arange(0.0, zmax, 0.01)
	nz_interp = np.interp(z_interp, z, nz)
	return (z_interp, nz_interp, spectro_fraction, 
		spectro_zmax, ngal, nbin,
		photo_zmax, photo_err, no_photo_err,
		)

def spectro_fraction_at_redshift(f, zmax, z):
	return f*np.tanh(6*(zmax-z)/zmax)

def add_spectro_fraction(z, nz_true, z_prob_matrix, spectro_fraction, spectro_zmax):
	#z_prob_matrix[i,j] = nz_true[j] * p(j->i)
	# The spectroscopic one is an identity matrix.
	# So the overall matrix will be the sum of the two components, but only up to zmax.
	n = len(z)
	for i in range(n):
		if z[i]>spectro_zmax: break
		s = z_prob_matrix[i,:].sum()
		f = spectro_fraction_at_redshift(spectro_fraction, spectro_zmax, z[i])
		p = 1-f
		z_prob_matrix[i,:] *= p
		z_prob_matrix[i,i] += f*s

def compute_nz(z, nz_true, nbin, ngal, sigma_z, spectro_fraction, spectro_zmax):
	# This is the photometric z matrix.
	z_prob_matrix = photo.photometric_error(z, nz_true, sigma_z, bias=0.0)
	add_spectro_fraction(z, nz_true, z_prob_matrix, spectro_fraction, spectro_zmax)
	edges = photo.find_bins(z,nz_true,nbin)
	bin_nz = photo.compute_bin_nz(z_prob_matrix, z, edges, ngal)
	return edges,bin_nz

def execute(block, config):
	(z, nz_true, spectro_fraction, spectro_zmax, ngal, nbin,
	 photo_zmax, photo_err, no_photo_err
		) = config
	params = section_names.number_density_params

	def sigz(z):
		if z<photo_zmax:
			return photo_err
		else:
			return no_photo_err

	#Run the main code for getting n(z) in bins
	edges,bins = compute_nz(z, nz_true, nbin, ngal, sigz, spectro_fraction, spectro_zmax)

	#Save the results
	nz_section = section_names.wl_number_density
	block[nz_section,"nbin"] = nbin
	block[nz_section,"nz"] = len(z)
	block[nz_section,"z"] = z

	#Loop through the bins
	for i,bin in enumerate(bins):
		#The bin numbering starts at 1
		b=i+1
		name = "BIN_%d" % b
		#And save the bin n(z) as a column
		block[nz_section, name] =  bin
	#Also save the bin edges to the top bin
	block[nz_section,"edges"] = edges

	return 0