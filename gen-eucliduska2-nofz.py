import numpy as np

dist_dir = 'cosmosis-ska-forecasting/distributions/'

ska2 = np.loadtxt(dist_dir+'nofz_ska2_3pi.dat')
euclid = np.loadtxt(dist_dir+'nofz_euclid_z10.dat')

overlap_fraction = np.loadtxt(dist_dir+'nofz_overlaps.dat')


ng_euclid = 30.e0
ng_ska2 = 10.e0

euclid_nz = ng_euclid*euclid[:,1]/np.trapz(euclid[:,1], euclid[:,0])
ska2_nz = ng_ska2*ska2[:,1]/np.trapz(ska2[:,1], ska2[:,0])

overlap_nz = np.append(overlap_fraction[:,1],
                       np.zeros(euclid_nz.shape[0]-overlap_fraction[:,1].shape[0]))
                       
cross_nz = euclid_nz + ska2_nz - overlap_nz*ska2_nz
ng_total = np.trapz(cross_nz, ska2[:,0])

np.savetxt(dist_dir+'nofz_euclidxska2.dat',
           np.column_stack([ska2[:,0], cross_nz]))

print(ng_total)
